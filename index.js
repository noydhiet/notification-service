const AppServer = require('./bin/app/server');
const config = require('./bin/config');

const appServer = new AppServer();
const port = process.env.port || config.get('/port') || 1337;

appServer.server.listen(port, () => {
  console.log(`Application started, listening at port ${port}`);
});