const sinon = require('sinon');
const validator = require('../../../../../bin/helpers/utils/validator');
const EmailNotif = require('../../../../../bin/modules/notif/repositories/email_repositories');
const emailHandler = require('../../../../../bin/modules/notif/handlers/email_handler');

describe('Email handler', () => {

  let stubValidator, stubEmailNotif;

  const req = {
    body: 'body'
  };

  const res = {
    status: () => {
      return {
        send: sinon.stub()
      };
    }
  };

  beforeEach(() => {
    stubValidator = sinon.stub(validator, 'isValidPayload');
    stubValidator.resolves({
      data: 'data'
    });
    stubEmailNotif = sinon.stub(EmailNotif.prototype, 'sendEmail');
    stubEmailNotif.resolves({
      data: 'data'
    });
  });

  afterEach(() => {
    validator.isValidPayload.restore();
    stubEmailNotif.restore();
    stubValidator.restore();
  });

  describe('sendEmail', () => {
    it('should success send email', () => {
      emailHandler.sendEmail(req, res);
    });
    it('should return error', () => {
      stubValidator.resolves({
        err: new Error('error')
      });
      emailHandler.sendEmail(req, res);
    });
  });
});
