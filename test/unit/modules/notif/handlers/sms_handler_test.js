const sinon = require('sinon');
const validator = require('../../../../../bin/helpers/utils/validator');
const SmsNotif = require('../../../../../bin/modules/notif/repositories/sms_repositories');
const smsHandler = require('../../../../../bin/modules/notif/handlers/sms_handler');

describe('Sms handler', () => {

  let stubValidator, stubSmsNotif;

  const req = {
    body: 'body'
  };

  const res = {
    status: () => {
      return {
        send: sinon.stub()
      };
    }
  };

  beforeEach(() => {
    stubValidator = sinon.stub(validator, 'isValidPayload');
    stubValidator.resolves({
      data: 'data'
    });
    stubSmsNotif = sinon.stub(SmsNotif.prototype, 'sendSmsNotif');
    stubSmsNotif.resolves({
      data: 'data'
    });
  });

  afterEach(() => {
    validator.isValidPayload.restore();
    stubSmsNotif.restore();
    stubValidator.restore();
  });

  describe('sendSmsNotif', () => {
    it('should success send emasmsil', () => {
      smsHandler.sendSmsNotif(req, res);
    });
    it('should return error', () => {
      stubValidator.resolves({
        err: new Error('error')
      });
      smsHandler.sendSmsNotif(req, res);
    });
  });
});
