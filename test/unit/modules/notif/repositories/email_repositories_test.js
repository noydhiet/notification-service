const nodemailer = require('nodemailer');
const sinon = require('sinon');
const assert = require('assert');
const EmailNotif = require('../../../../../bin/modules/notif/repositories/email_repositories');

describe('Email Repositories', () => {

  let emailStub;

  const payload = {
    subject: 'subject',
    email: 'email',
    text: 'text'
  };

  beforeEach(() => {
    emailStub = sinon.stub(nodemailer, 'createTransport');
    emailStub.returns({
      sendMail: () => {
        return  { accepted: true };
      }
    });
  });

  afterEach(() => {
    emailStub.restore();
  });

  describe('sendEmail', () => {
    it('should success send email', async() => {
      const emailNotif = new EmailNotif();
      const result = await emailNotif.sendEmail(payload);
      assert.equal(result.data, 'Send Email successfull');
    });
    it('should return error', async() => {
      emailStub.returns({
        sendMail: () => {
          return  {};
        }
      });
      const emailNotif = new EmailNotif();
      const result = await emailNotif.sendEmail(payload);
      assert.notEqual(result.err, null);
    });
  });
});
