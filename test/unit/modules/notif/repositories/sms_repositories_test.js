const rp = require('request-promise');
const sinon = require('sinon');
const assert = require('assert');
const SmsNotif = require('../../../../../bin/modules/notif/repositories/sms_repositories');
const logger = require('../../../../../bin/helpers/utils/logger');

describe('Sms Repositories', () => {

  let rpStub, rpStubPost;

  const payload = {
    message: 'message',
    phone: 'phone'
  };

  beforeEach(() => {
    rpStub = sinon.stub(rp, 'get');
    rpStub.resolves({
      data: 'data'
    });
    rpStubPost = sinon.stub(rp, 'post');
    rpStubPost.resolves({
      ok: true
    });
    sinon.stub(logger, 'log');
  });

  afterEach(() => {
    rpStub.restore();
    rpStubPost.restore();
    logger.log.restore();
  });

  describe('getAccessToken', () => {
    it('should return access token', async() => {
      const smsNotif = new SmsNotif();
      const rs = await smsNotif.getAccessToken();
      assert.equal(rs.data, 'data');
    });

    it('should return auth error', async() => {
      const smsNotif = new SmsNotif();
      rpStub.resolves({
        ok: false
      });
      const rs = await smsNotif.getAccessToken();
      assert.notEqual(rs.err, null);
    });

    it('should return api error', async() => {
      const smsNotif = new SmsNotif();
      rpStub.throws(new Error('Error'));
      const rs = await smsNotif.getAccessToken();
      assert.notEqual(rs.err, null);
    });
  });

  describe('sendSmsNotif', () => {
    it('should succes send sms', async() => {
      const smsNotif = new SmsNotif();
      const rs = await smsNotif.sendSmsNotif(payload);
      assert.equal(rs.data, 'Send SMS successfull');
    });
    it('should return error indihome', async() => {
      rpStubPost.resolves({});
      const smsNotif = new SmsNotif();
      const rs = await smsNotif.sendSmsNotif(payload);
      assert.equal(rs.err, 'Error: Indihome service connection lost');
    });
    it('should failed send sms', async() => {
      rpStubPost.resolves({
        ok: false
      });
      const smsNotif = new SmsNotif();
      const rs = await smsNotif.sendSmsNotif(payload);
      assert.equal(rs.err, 'Error: Send SMS failed');
    });
    it('should return api error', async() => {
      rpStubPost.throws(new Error('error'));
      const smsNotif = new SmsNotif();
      const rs = await smsNotif.sendSmsNotif(payload);
      assert.equal(rs.err, 'Error: error');
    });
    it('should return access token error', async() => {
      rpStub.throws(new Error('error'));
      const smsNotif = new SmsNotif();
      const rs = await smsNotif.sendSmsNotif(payload);
      assert.equal(rs.err, 'Error: error');
    });
  });
});
