const sinon = require('sinon');

const wrapper = require('../../../../bin/helpers/utils/wrapper');
const { NotFoundError, InternalServerError, ConflictError, BadRequestError, ConditionNotMetError,
  ForbiddenError, MethodNotAllowedError, UnauthorizedError } = require('../../../../bin/helpers/error');

describe('Wrapper', () => {

  const res = { status: () => {
    return {
      send: sinon.stub()
    };
  } };

  describe('response', () => {
    it('should cover branch', () => {
      wrapper.response(res, 'success', { data: {}, meta: {}});
    });
  });

  describe('checkErrorCode', () => {
    it('should return NotFoundError', () => {
      wrapper.response(res, 'fail', { err: new NotFoundError()});
    });
    it('should return BadRequestError', () => {
      wrapper.response(res, 'fail', { err: new BadRequestError()});
    });
    it('should return InternalServerError', () => {
      wrapper.response(res, 'fail', { err: new InternalServerError()});
    });
    it('should return ConflictError', () => {
      wrapper.response(res, 'fail', { err: new ConflictError()});
    });
    it('should return ExpectationFailedError', () => {
      wrapper.response(res, 'fail', { err: new ConditionNotMetError()});
    });
    it('should return GatewayTimeoutError', () => {
      wrapper.response(res, 'fail', { err: new MethodNotAllowedError()});
    });
    it('should return UnauthorizedError', () => {
      wrapper.response(res, 'fail', { err: new UnauthorizedError()});
    });
    it('should return ForbiddenError', () => {
      wrapper.response(res, 'fail', { err: new ForbiddenError()});
    });
  });
});
