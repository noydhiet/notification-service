// const hippie = require('hippie');
// const assert = require('assert');
// const sinon = require('sinon');
// const AppServer = require('../../bin/app/server');
// const SmsNotif = require('../../bin/modules/notif/repositories/sms_repositories');
// const smsHandler = require('../../bin/modules/notif/handlers/sms_handler');

// describe('Post Sms Notif', () => {

//   let appServer;

//   let payload = {
//     'phone': '08219296976',
//     'message': 'Halo world'
//   };

//   let result = {
//     data: 'data'
//   };

//   beforeEach(function () {
//     appServer = new AppServer();
//     this.server = appServer.server;
//   });

//   it('Should access sms notif service', function (done) {
//     sinon.stub(smsHandler, 'sendSmsNotif');
//     sinon.stub(SmsNotif.prototype, 'sendSmsNotif').resolves(result);

//     hippie(appServer.server)
//       .header('authorization', 'Basic bXlJbmRpaG9tZVg6Nkw3MUxPdWlubGloOWJuWkhBSUtKMjFIc3Qxcg==')
//       .json()
//       .post('/sms-notif')
//       .send(payload)
//       .expectStatus(200)
//       .end((err, res, body) => {
//         if(err){
//           throw err;
//         }
//         console.log(body);
//         done();
//       });

//       smsHandler.sendSmsNotif.restore();
//       SmsNotif.prototype.sendSmsNotif.restore();
//   });

// });
