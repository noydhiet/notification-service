const fcm = require('firebase-admin');
const config = require('../../../config');
const connectionPool = [];

const init = async () => {
  const client = fcm.initializeApp({
    credential: fcm.credential.cert(config.get('/firebase')),
    databaseURL: 'https://myindihome-pccw.firebaseio.com'
  });
  connectionPool.push(client);
  return connectionPool;
};

const getConnectFirebase = async () => {
  return connectionPool;
};
module.exports = {
  init,
  getConnectFirebase
};
