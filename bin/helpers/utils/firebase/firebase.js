const getConnection = require('./connection');
const wrapper = require('../wrapper');
const logger = require('../logger');

const sendToDevice = async (tokens, payload, options) => {
  let connections = await getConnection.getConnectFirebase();
  try {
    const multicastMessage = {
      ...payload,
      tokens,
    };
    const batchResponse = await connections[0].messaging().sendMulticast(multicastMessage);
    if (batchResponse.failureCount > 0) {
      const failedTokens = [];
      batchResponse.responses.forEach((resp, index) => {
        if (!resp.success) {
          failedTokens.push(tokens[index]);
        }
      });
      logger.log('firebase-sendToDevice',`List of tokens that caused failures: ${JSON.stringify(failedTokens)}`,'FCM response');
    }
    logger.log('firebase-sendToDevice', JSON.stringify(batchResponse), 'FCM response');
    return wrapper.data(batchResponse);
  } catch (err) {
    logger.log('firebase-sendToDevice', err.message, 'error send to device');
    logger.log('firebase-sendToDevice', JSON.stringify(err), `error sending multicast to devices: [${JSON.stringify(tokens)}]`);
    return wrapper.error(err);
  }
};

const sendToTopic = async (topic, payload, options) => {
  let connections = await getConnection.getConnectFirebase();
  try {
    const message = await connections[0].messaging().sendToTopic(topic, payload, options);
    if (message) {
      return wrapper.data(message);
    }
  } catch (err) {
    logger.log('firebase-sendToDevice', err.message, 'error send to device');
    return wrapper.error(err);
  }
};

module.exports = {
  sendToDevice,
  sendToTopic
};
