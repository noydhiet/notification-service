const joi = require('joi');
const validate = require('validate.js');
const { BadRequestError } = require('../error');
const wrapper = require('./wrapper');

const isValidPayload = (payload, constraint) => {
  const { value, error } = joi.validate(payload, constraint);
  if(!validate.isEmpty(error)){
    return wrapper.error(new BadRequestError(error));
  }
  return wrapper.data(value);

};

module.exports = {
  isValidPayload
};
