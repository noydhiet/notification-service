const {
  NotFoundError, InternalServerError, BadRequestError, ConflictError, ConditionNotMetError,
  ForbiddenError, MethodNotAllowedError, UnauthorizedError,
} = require('../error');
const { ERROR: httpError } = require('../http-status/status_code');

const data = (data) => ({ err: null, data });

const error = (err) => ({ err, data: null });

const response = (res, type, result, message = '', code = 200) => {
  let status = true;
  let { data } = result;
  if (type === 'fail') {
    status = false;
    data = '';
    message = result.err.message || message;
    code = checkErrorCode(result.err);
  }
  res.status(code).send({
    ok: status,
    data,
    message,
    status: code,
  });
};

const checkErrorCode = (error) => {
  switch (error.constructor) {
  case BadRequestError:
    return httpError.BAD_REQUEST;
  case ConflictError:
    return httpError.CONFLICT;
  case ConditionNotMetError:
    return httpError.CONDITION_NOT_MET;
  case ForbiddenError:
    return httpError.FORBIDDEN;
  case InternalServerError:
    return httpError.INTERNAL_ERROR;
  case NotFoundError:
    return httpError.NOT_FOUND;
  case MethodNotAllowedError:
    return httpError.METHOD_NOT_ALLOWED;
  case UnauthorizedError:
    return httpError.UNAUTHORIZED;
  default:
    return httpError.CONFLICT;
  }
};

module.exports = {
  data,
  error,
  response,
};
