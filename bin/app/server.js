const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');
const basicAuth = require('../auth/basic_auth_helper');
const jwtAuth = require('../auth/jwt_auth_helper');
const wrapper = require('../helpers/utils/wrapper');
const smsHandler = require('../modules/notif/handlers/sms_handler');
const emailHandler = require('../modules/notif/handlers/email_handler');
const swaggerDocument = require('../swagger/swagger.json');
const notifHandler = require('../modules/notif/handlers/notification_handler');
const initFirebase = require('../helpers/utils/firebase/connection');

function AppServer() {
  this.server = express();

  this.server.use(cors());
  this.server.use(bodyParser.urlencoded({ extended: false }));
  this.server.use(bodyParser.json());

  // required for basic auth
  this.server.use(basicAuth.init());

  this.server.get('/', (req, res) => {
    wrapper.response(res, 'success', wrapper.data('User service'), 'This service is running properly.');
  });

  this.server.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

  this.server.post('/users/notification/sms', basicAuth.isAuthenticated, smsHandler.sendSmsNotif);
  this.server.post('/users/notification/email', basicAuth.isAuthenticated, emailHandler.sendEmail);
  this.server.post('/users/notification/push', basicAuth.isAuthenticated, notifHandler.sendNotif);
  this.server.post('/channels/notification/push', basicAuth.isAuthenticated, notifHandler.sendNotifToChannel);
  this.server.post('/users/notificationByUser', basicAuth.isAuthenticated, notifHandler.sendNotifToUser);

  initFirebase.init();
}

module.exports = AppServer;
