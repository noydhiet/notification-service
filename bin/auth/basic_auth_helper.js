
const passport = require('passport');
const { BasicStrategy } = require('passport-http');
const User = require('./auth_repository');
const wrapper = require('../helpers/utils/wrapper');
const { UnauthorizedError } = require('../helpers/error');

passport.use(new BasicStrategy((username, password, cb) => {
  User.findByUsername(username, (user) => {
    if (!user) {
      return cb(null, false);
    }
    if (!user.isValidPassword(password)) {
      return cb(null, false);
    }
    return cb(null, user);

  });
}));

const isAuthenticated = (req, res, next) => {
  passport.authenticate('basic', { session: false }, (err, user, info) => {
    const error = {
      err: new UnauthorizedError
    };
    if (err) { return wrapper.response(res, 'fail', error); }
    if (!user) { return wrapper.response(res, 'fail', error); }
    next();
  })(req, res, next);
};
const init = () => passport.initialize();

module.exports = {
  isAuthenticated,
  init
};
