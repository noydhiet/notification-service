const joi = require('joi');

const emailNotif = joi.object({
  emails: joi.array().items(joi.string()).required(),
  subject: joi.string().required(),
  text: joi.string().required()
});

module.exports = {
  emailNotif
};
