const joi = require('joi');

const smsNotif = joi.object({
  phones: joi.array().items(joi.string()).required(),
  message: joi.string().required()
});

module.exports = {
  smsNotif
};
