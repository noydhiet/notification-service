const joi = require('joi');

const notification = joi.object({
  deviceTokens: joi.array().items(joi.string()).required(),
  message: joi.object().required()
});

const notificationToChannel = joi.object({
  topic: joi.string().required(),
  message: joi.object().required()
});

const notificationByUser = joi.object({
  userId: joi.string().required(),
  message: joi.object().required()
});

module.exports = {
  notification,
  notificationToChannel,
  notificationByUser
};
