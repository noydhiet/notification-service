const rp = require('request-promise');
const validate = require('validate.js');
const wrapper = require('../../../helpers/utils/wrapper');
const logger = require('../../../helpers/utils/logger');
const config = require('../../../config');
const { InternalServerError, ConditionNotMetError } = require('../../../helpers/error');

class SmsNotif{

  async getAccessToken() {
    const ctx = 'repositories-getAccessToken';
    const [ authUser ] = config.get('/basicAuthApi');
    const options = {
      method: 'GET',
      uri: `${config.get('/authenticationBaseUrl')}/user/access-token`,
      auth: {
        user: authUser.username,
        pass: authUser.password
      },
      headers: {
        'Content-Type': 'application/json',
      },
      json: true,
    };

    try {
      const result = await rp.get(options);
      if (result.ok===false) {
        const msg = new InternalServerError('Authentication service connection lost');
        logger.log(ctx, msg, 'Authentication service connection lost');
        return wrapper.error(msg);
      }
      return wrapper.data(result.data);
    } catch (err) {
      logger.log(ctx, err, 'API connection error');
      return wrapper.error(err);
    }
  }

  async sendSmsNotif(payload){
    const ctx = 'repositories-sendSmsNotif';
    const { message, phones } = payload;
    const tokenData = await this.getAccessToken();
    if(tokenData.err) {
      return wrapper.error(tokenData.err);
    }

    const options = {
      method: 'POST',
      uri: `${config.get('/indihomeBaseUrl')}/gateway/smsBulkGeneral2/1.0/smsBulkGeneral2`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${tokenData.data.token}`
      },
      body: {
        senderid: config.get('/indihomeSenderId'),
        username: config.get('/indihomeUsername'),
        password: config.get('/indihomePassword'),
        msisdn: phones,
        message: message
      },
      json: true,
    };

    try {
      const result = await rp.post(options);
      if (validate.isEmpty(result.ok)) {
        const msg = new InternalServerError('Indihome service connection lost');
        logger.log(ctx, msg, 'Indihome service connection lost');
        return wrapper.error(msg);
      }
      if (result.ok===false) {
        const msg = new ConditionNotMetError('Send SMS failed');
        logger.log(ctx, msg, 'Send SMS failed');
        return wrapper.error(msg);
      }
      return wrapper.data('Send SMS successfull');
    } catch (err) {
      logger.log(ctx, err, 'API connection error');
      return wrapper.error(err);
    }
  }

}

module.exports = SmsNotif;
