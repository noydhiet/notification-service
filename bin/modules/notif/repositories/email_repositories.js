const validate = require('validate.js');
const nodemailer = require('nodemailer');
const wrapper = require('../../../helpers/utils/wrapper');
const config = require('../../../config');
const { ConflictError } = require('../../../helpers/error');

class EmailNotif {

  async sendEmail(payload) {
    const { emails, subject, text } = payload;
    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: config.get('/emailUsername'),
        pass: config.get('/emailPassword')
      }
    });

    const mailOptions = {
      from: `"Indihome" <${config.get('/emailUsername')}>`, // sender address
      to: emails, // list of receivers
      subject, // Subject line
      html: text // plain text body
    };

    const result = await transporter.sendMail(mailOptions);
    if(!validate.isEmpty(result.rejected) || validate.isEmpty(result.accepted)) {
      return wrapper.error(new ConflictError('Send email failed'));
    }

    return wrapper.data('Send Email successfull');
  }
}

module.exports = EmailNotif;

