const rp = require('request-promise');
const wrapper = require('../../../helpers/utils/wrapper');
const { ConflictError, InternalServerError } = require('../../../helpers/error');
const firebase = require('../../../helpers/utils/firebase/firebase');
const logger = require('../../../helpers/utils/logger');
const config = require('../../../config');

class Notification {

  async sendNotif(payload) {
    const { deviceTokens, message } = payload;
    const messages = message;
    const options = {
      priority: 'high',
      timeToLive: 60 * 60 * 24
    };

    const result = await firebase.sendToDevice(deviceTokens, messages, options);
    if(result.err){
      logger.log('sendNotifFirebase', result.err, 'Send notif failed');
      return wrapper.error(new ConflictError('Send notif failed'));
    }
    logger.log('sendNotifFirebase', 'success', `sent push notification as: ${JSON.stringify(payload)}`);
    logger.log('sendNotifFirebase', 'success', `sent push notification response from firebase as: ${JSON.stringify(result)}`);
    return wrapper.data(result.data);
  }

  async sendNotifToChannel(payload) {
    const { topic, message } = payload;
    const messages = message;
    const options = {
      priority: 'high',
      timeToLive: 60 * 60 * 24
    };

    const result = await firebase.sendToTopic(topic, messages, options);
    if(result.err){
      logger.log('sendNotifFirebase', result.err, 'Send notif failed');
      return wrapper.error(new ConflictError('Send notif failed'));
    }
    logger.log('sendNotifFirebase', 'success', 'Send notif success');
    return wrapper.data('Send notif successfull');
  }

  async sendNotifByUser(payload) {
    const { userId, message } = payload;
    const [ authUser ] = config.get('/basicAuthApi');
    const messages = message;
    const options = {
      priority: 'high',
      timeToLive: 60 * 60 * 24
    };

    //getDeviceId
    const getDevice = {
      method: 'GET',
      uri: `${config.get('/identityBaseUrl')}:${config.get('/authenticationPort')}/account/device/`+userId,
      auth: {
        user: authUser.username,
        pass: authUser.password
      },
      headers: {
        'Content-Type': 'application/json',
      },
      json: true,
    };

    try {
      const getDeviceId = await rp.get(getDevice);
      if (getDeviceId.ok===false) {
        const msg = new InternalServerError('Authentication service connection lost');
        return wrapper.error(msg);
      }

      const result = await firebase.sendToDevice(getDeviceId.data.deviceId, messages, options);
      if(result.err){
        logger.log('sendNotifFirebase', result.err, 'Send notif failed');
        return wrapper.error(new ConflictError('Send notif failed'));
      }
      logger.log('sendNotifFirebase', 'success', 'Send notif success');
      return wrapper.data('Send notif successfull');
    } catch (err) {
      return wrapper.error(err);
    }
  }
}

module.exports = Notification;

