const wrapper = require('../../../helpers/utils/wrapper');
const SmsNotif = require('../repositories/sms_repositories');
const model = require('../domains/sms');
const validator = require('../../../helpers/utils/validator');

const sendSmsNotif = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, model.smsNotif);
  const smsNotif = new SmsNotif();
  const postData = async (result) => {
    if(result.err){
      return result;
    }
    return smsNotif.sendSmsNotif(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Send SMS Notif')
      : wrapper.response(res, 'success', result, 'Send SMS Notif');
  };
  sendResponse(await postData(await validatePayload));
};

module.exports = {
  sendSmsNotif
};
