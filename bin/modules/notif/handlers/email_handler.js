const wrapper = require('../../../helpers/utils/wrapper');
const EmailNotif = require('../repositories/email_repositories');
const model = require('../domains/email');
const validator = require('../../../helpers/utils/validator');

const sendEmail = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, model.emailNotif);
  const emailNotif = new EmailNotif();
  const postData = async (result) => {
    if(result.err) {
      return result;
    }
    return emailNotif.sendEmail(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Send Email Notif')
      : wrapper.response(res, 'success', result, 'Send Email Notif');
  };
  sendResponse(await postData(await validatePayload));
};

module.exports = {
  sendEmail
};
