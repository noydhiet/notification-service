const wrapper = require('../../../helpers/utils/wrapper');
const Notification = require('../repositories/notification_repositories');
const model = require('../domains/notification');
const validator = require('../../../helpers/utils/validator');

const sendNotif = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, model.notification);
  const notification = new Notification();
  const postData = async (result) => {
    if(result.err) {
      return result;
    }
    return notification.sendNotif(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Send Notif Failed')
      : wrapper.response(res, 'success', result, 'Send Notif Success');
  };
  sendResponse(await postData(await validatePayload));
};

const sendNotifToChannel = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, model.notificationToChannel);
  const notification = new Notification();
  const postData = async (result) => {
    if(result.err) {
      return result;
    }
    return notification.sendNotifToChannel(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Send Notif Failed')
      : wrapper.response(res, 'success', result, 'Send Notif Success');
  };
  sendResponse(await postData(await validatePayload));
};

const sendNotifToUser = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, model.notificationByUser);
  const notification = new Notification();
  const postData = async (result) => {
    if(result.err) {
      return result;
    }
    return notification.sendNotifByUser(payload);
  };
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Send Notif Failed')
      : wrapper.response(res, 'success', result, 'Send Notif Success');
  };
  sendResponse(await postData(await validatePayload));


}
module.exports = {
  sendNotif,
  sendNotifToChannel,
  sendNotifToUser
};
