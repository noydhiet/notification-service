# Core Notification Service

[![pipeline status](https://gitlab.mihpccw.com/core-services/notification-service/badges/master/pipeline.svg)](https://gitlab.mihpccw.com/core-services/notification-service/commits/master)

[![coverage report](https://gitlab.mihpccw.com/core-services/notification-service/badges/master/coverage.svg)](https://gitlab.mihpccw.com/core-services/notification-service/commits/master)

Core Notification Service which provides SMS and EMail sending features for PCCW identity server logged in users.